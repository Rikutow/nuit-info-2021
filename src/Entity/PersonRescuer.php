<?php

namespace App\Entity;

use App\Repository\PersonRescuerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRescuerRepository::class)
 */
class PersonRescuer extends Person
{
    /**
     * @ORM\ManyToMany(targetEntity=BoatRescuer::class, mappedBy="rescuerPersons")
     */
    private $boatRescuers;

    public function __construct()
    {
        $this->boatRescuers = new ArrayCollection();
    }

    public function getType(): string
    {
        return parent::PERSON_RESCUED;
    }

    /**
     * @return Collection|BoatRescuer[]
     */
    public function getBoatRescuers(): Collection
    {
        return $this->boatRescuers;
    }

    public function addBoatRescuer(BoatRescuer $boatRescuer): self
    {
        if (!$this->boatRescuers->contains($boatRescuer)) {
            $this->boatRescuers[] = $boatRescuer;
            $boatRescuer->addRescuerPerson($this);
        }

        return $this;
    }

    public function removeBoatRescuer(BoatRescuer $boatRescuer): self
    {
        if ($this->boatRescuers->removeElement($boatRescuer)) {
            $boatRescuer->removeRescuerPerson($this);
        }

        return $this;
    }
}
