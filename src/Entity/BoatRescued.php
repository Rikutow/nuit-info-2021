<?php

namespace App\Entity;

use App\Repository\BoatRescuedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BoatRescuedRepository::class)
 */
class BoatRescued extends Boat
{
    /**
     * @ORM\ManyToMany(targetEntity=RescueMission::class, mappedBy="rescuedBoats")
     */
    private $rescueMissions;

    /**
     * @ORM\ManyToMany(targetEntity=PersonRescued::class, inversedBy="boatRescueds")
     */
    private $rescuedPersons;

    public function __construct()
    {
        $this->rescueMissions = new ArrayCollection();
        $this->rescuedPersons = new ArrayCollection();
    }

    public function getType(): string
    {
        return parent::BOAT_RESCUED;
    }

    /**
     * @return Collection|RescueMission[]
     */
    public function getRescueMissions(): Collection
    {
        return $this->rescueMissions;
    }

    public function addRescueMission(RescueMission $rescueMission): self
    {
        if (!$this->rescueMissions->contains($rescueMission)) {
            $this->rescueMissions[] = $rescueMission;
            $rescueMission->addRescuedBoat($this);
        }

        return $this;
    }

    public function removeRescueMission(RescueMission $rescueMission): self
    {
        if ($this->rescueMissions->removeElement($rescueMission)) {
            $rescueMission->removeRescuedBoat($this);
        }

        return $this;
    }

    /**
     * @return Collection|PersonRescued[]
     */
    public function getRescuedPersons(): Collection
    {
        return $this->rescuedPersons;
    }

    public function addRescuedPerson(PersonRescued $rescuedPerson): self
    {
        if (!$this->rescuedPersons->contains($rescuedPerson)) {
            $this->rescuedPersons[] = $rescuedPerson;
        }

        return $this;
    }

    public function removeRescuedPerson(PersonRescued $rescuedPerson): self
    {
        $this->rescuedPersons->removeElement($rescuedPerson);

        return $this;
    }
}
