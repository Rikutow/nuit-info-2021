<?php

namespace App\Entity;

use App\Repository\RescueMissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RescueMissionRepository::class)
 */
class RescueMission
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Rescue::class, inversedBy="rescueMissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rescue;

    /**
     * @ORM\ManyToMany(targetEntity=BoatRescuer::class, inversedBy="rescueMissions")
     */
    private $rescuerBoats;

    /**
     * @ORM\ManyToMany(targetEntity=BoatRescued::class, inversedBy="rescueMissions")
     */
    private $rescuedBoats;

    public function __construct()
    {
        $this->rescuerBoats = new ArrayCollection();
        $this->rescuedBoats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getRescue(): ?Rescue
    {
        return $this->rescue;
    }

    public function setRescue(?Rescue $rescue): self
    {
        $this->rescue = $rescue;

        return $this;
    }

    /**
     * @return Collection|BoatRescuer[]
     */
    public function getRescuerBoats(): Collection
    {
        return $this->rescuerBoats;
    }

    public function addRescuerBoat(BoatRescuer $rescuerBoat): self
    {
        if (!$this->rescuerBoats->contains($rescuerBoat)) {
            $this->rescuerBoats[] = $rescuerBoat;
        }

        return $this;
    }

    public function removeRescuerBoat(BoatRescuer $rescuerBoat): self
    {
        $this->rescuerBoats->removeElement($rescuerBoat);

        return $this;
    }

    /**
     * @return Collection|BoatRescued[]
     */
    public function getRescuedBoats(): Collection
    {
        return $this->rescuedBoats;
    }

    public function addRescuedBoat(BoatRescued $rescuedBoat): self
    {
        if (!$this->rescuedBoats->contains($rescuedBoat)) {
            $this->rescuedBoats[] = $rescuedBoat;
        }

        return $this;
    }

    public function removeRescuedBoat(BoatRescued $rescuedBoat): self
    {
        $this->rescuedBoats->removeElement($rescuedBoat);

        return $this;
    }
}
