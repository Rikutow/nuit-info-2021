<?php

namespace App\Entity;

use App\Repository\RescueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RescueRepository::class)
 */
class Rescue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=RescueMission::class, mappedBy="rescue", orphanRemoval=true)
     */
    private $rescueMissions;

    public function __construct()
    {
        $this->rescueMissions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|RescueMission[]
     */
    public function getRescueMissions(): Collection
    {
        return $this->rescueMissions;
    }

    public function addRescueMission(RescueMission $rescueMission): self
    {
        if (!$this->rescueMissions->contains($rescueMission)) {
            $this->rescueMissions[] = $rescueMission;
            $rescueMission->setRescue($this);
        }

        return $this;
    }

    public function removeRescueMission(RescueMission $rescueMission): self
    {
        if ($this->rescueMissions->removeElement($rescueMission)) {
            // set the owning side to null (unless already changed)
            if ($rescueMission->getRescue() === $this) {
                $rescueMission->setRescue(null);
            }
        }

        return $this;
    }
}
