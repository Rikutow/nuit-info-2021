<?php

namespace App\Entity;

use App\Repository\BoatRescuerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BoatRescuerRepository::class)
 */
class BoatRescuer extends Boat
{
    /**
     * @ORM\ManyToMany(targetEntity=RescueMission::class, mappedBy="rescuerBoats")
     */
    private $rescueMissions;

    /**
     * @ORM\ManyToMany(targetEntity=PersonRescuer::class, inversedBy="boatRescuers")
     */
    private $rescuerPersons;

    public function __construct()
    {
        $this->rescueMissions = new ArrayCollection();
        $this->rescuerPersons = new ArrayCollection();
    }

    public function getType(): string
    {
        return parent::BOAT_RESCUER;
    }

    /**
     * @return Collection|RescueMission[]
     */
    public function getRescueMissions(): Collection
    {
        return $this->rescueMissions;
    }

    public function addRescueMission(RescueMission $rescueMission): self
    {
        if (!$this->rescueMissions->contains($rescueMission)) {
            $this->rescueMissions[] = $rescueMission;
            $rescueMission->addRescuerBoat($this);
        }

        return $this;
    }

    public function removeRescueMission(RescueMission $rescueMission): self
    {
        if ($this->rescueMissions->removeElement($rescueMission)) {
            $rescueMission->removeRescuerBoat($this);
        }

        return $this;
    }

    /**
     * @return Collection|PersonRescuer[]
     */
    public function getRescuerPersons(): Collection
    {
        return $this->rescuerPersons;
    }

    public function addRescuerPerson(PersonRescuer $rescuerPerson): self
    {
        if (!$this->rescuerPersons->contains($rescuerPerson)) {
            $this->rescuerPersons[] = $rescuerPerson;
        }

        return $this;
    }

    public function removeRescuerPerson(PersonRescuer $rescuerPerson): self
    {
        $this->rescuerPersons->removeElement($rescuerPerson);

        return $this;
    }
}
