<?php

namespace App\Entity;

use App\Repository\PersonRescuedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRescuedRepository::class)
 */
class PersonRescued extends Person
{
    /**
     * @ORM\ManyToMany(targetEntity=BoatRescued::class, mappedBy="rescuedPersons")
     */
    private $boatRescueds;

    public function __construct()
    {
        $this->boatRescueds = new ArrayCollection();
    }

    public function getType(): string
    {
        return parent::PERSON_RESCUED;
    }

    /**
     * @return Collection|BoatRescued[]
     */
    public function getBoatRescueds(): Collection
    {
        return $this->boatRescueds;
    }

    public function addBoatRescued(BoatRescued $boatRescued): self
    {
        if (!$this->boatRescueds->contains($boatRescued)) {
            $this->boatRescueds[] = $boatRescued;
            $boatRescued->addRescuedPerson($this);
        }

        return $this;
    }

    public function removeBoatRescued(BoatRescued $boatRescued): self
    {
        if ($this->boatRescueds->removeElement($boatRescued)) {
            $boatRescued->removeRescuedPerson($this);
        }

        return $this;
    }
}
