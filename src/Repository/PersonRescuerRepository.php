<?php

namespace App\Repository;

use App\Entity\PersonRescuer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonRescuer|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonRescuer|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonRescuer[]    findAll()
 * @method PersonRescuer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRescuerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonRescuer::class);
    }

    // /**
    //  * @return PersonRescuer[] Returns an array of PersonRescuer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonRescuer
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
