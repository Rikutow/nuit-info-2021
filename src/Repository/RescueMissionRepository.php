<?php

namespace App\Repository;

use App\Entity\RescueMission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RescueMission|null find($id, $lockMode = null, $lockVersion = null)
 * @method RescueMission|null findOneBy(array $criteria, array $orderBy = null)
 * @method RescueMission[]    findAll()
 * @method RescueMission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RescueMissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RescueMission::class);
    }

    // /**
    //  * @return RescueMission[] Returns an array of RescueMission objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RescueMission
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
