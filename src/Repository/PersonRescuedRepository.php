<?php

namespace App\Repository;

use App\Entity\PersonRescued;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonRescued|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonRescued|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonRescued[]    findAll()
 * @method PersonRescued[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRescuedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonRescued::class);
    }

    // /**
    //  * @return PersonRescued[] Returns an array of PersonRescued objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonRescued
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
