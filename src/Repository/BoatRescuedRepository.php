<?php

namespace App\Repository;

use App\Entity\BoatRescued;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BoatRescued|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoatRescued|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoatRescued[]    findAll()
 * @method BoatRescued[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoatRescuedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoatRescued::class);
    }

    // /**
    //  * @return BoatRescued[] Returns an array of BoatRescued objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoatRescued
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
