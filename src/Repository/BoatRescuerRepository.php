<?php

namespace App\Repository;

use App\Entity\BoatRescuer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BoatRescuer|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoatRescuer|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoatRescuer[]    findAll()
 * @method BoatRescuer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoatRescuerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoatRescuer::class);
    }

    // /**
    //  * @return BoatRescuer[] Returns an array of BoatRescuer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoatRescuer
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
