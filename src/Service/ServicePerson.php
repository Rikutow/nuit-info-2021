<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\PersonRepository;
use App\Entity\Person;

class ServicePerson extends ServiceBase
{    
    private $personRepository;
    
    public function __construct(EntityManagerInterface $em, PersonRepository $personRepository)
    {
        parent::__construct($em);        
        $this->personRepository = $personRepository;
    }
}