<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\UserRepository;
use App\Entity\User;

class ServiceUser extends ServiceBase
{    
    private $userRepository;
    private $passwordEncoder;
    
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($em);        
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function encodePassword(User $user): void
    {
        $password = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);
    }    
    
    public function getAllUsers(): array
    {
        return $this->userRepository->findBy([], ['username' =>  'ASC']);
    }

    public function getUserById(int $userId): ?User
    {
        return $this->userRepository->find($userId);
    }

    // public function toggleRole(User $user, string $role): void
    // {
    //     $user->toggleRole($role);
    //     $$this->save($user);
    // }

    // public function toggleActivation(User $user): void
    // {
    //     $user->setIsActivated(!$user->getIsActivated());
    //     $this->save($user);
    // }
}