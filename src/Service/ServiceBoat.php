<?php
namespace App\Service;

use App\Entity\Boat;
use App\Repository\BoatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ServiceBoat extends ServiceBase
{    
    private $boatRepository;
    
    public function __construct(EntityManagerInterface $em, BoatRepository $boatRepository)
    {
        parent::__construct($em);        
        $this->boatRepository = $boatRepository;
    }

    public function getAll(): array
    {
        return $this->boatRepository->findAll();
    }
}