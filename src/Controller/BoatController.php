<?php

namespace App\Controller;

use App\Entity\Boat;
use App\Form\BoatType;
use App\Form\PersonType;
use App\Entity\BoatRescued;
use App\Entity\BoatRescuer;
use App\Service\ServiceBoat;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BoatController extends AbstractController
{
    /**
     * @Route("/created-boats", name="app_created_boats")
     */
    public function createdBoats(ServiceBoat $serviceBoat): Response
    {
        $boats = $serviceBoat->getAll();
        return $this->render('app/boat/created_boats.html.twig', [
            'boats' => $boats
        ]);
    }

    /**
     * @Route("/create-boat/{boat}", name="app_create_boat")
     */
    public function createBoat(Boat $boat = null, Request $request, ServiceBoat $serviceBoat): Response
    {
        if (!$boat) {
            $entity = $request->query->get('entity');
            if ($entity === 'rescuer') $boat = new BoatRescuer();
            if ($entity === 'rescued') $boat = new BoatRescued();
        }

        if ($boat) {
            $form = $this->createForm(BoatType::class, $boat);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $serviceBoat->save($boat);
                $this->addFlash(
                    'success',
                    'Le bateau a été crée avec succès'
                );
                return $this->redirectToRoute('app_created_boats');
            }
        }

        return $this->render('app/boat/create_boat.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
