<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\ServiceUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error) $this->addFlash('error', $error->getMessageKey());
        
        return $this->render('vitrine/security/login.html.twig', ['last_username' => $lastUsername]);
    }

    /**
     * @Route("/signup", name="app_signup")
     */
    public function signup(Request $request, ServiceUser $serviceUser): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['mode' => UserType::REGISTER]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $serviceUser->encodePassword($user);
            $serviceUser->save($user);
            $this->addFlash(
                'success',
                'Le compte a été crée avec succès'
            );
            return $this->redirectToRoute('app_login');
        }

        return $this->render('vitrine/security/signup.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('app_login');
    }
}
