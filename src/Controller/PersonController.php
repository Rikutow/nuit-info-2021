<?php

namespace App\Controller;

use App\Form\PersonType;
use App\Entity\PersonRescued;
use App\Entity\PersonRescuer;
use App\Service\ServicePerson;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PersonController extends AbstractController
{
    /**
     * @Route("/persons", name="app_create_person")
     */
    public function createPerson(Request $request, ServicePerson $servicePerson): Response
    {
        $entity = $request->query->get('entity');
        $person = null;
        if ($entity === 'rescuer') $person = new PersonRescuer();
        if ($entity === 'rescued') $person = new PersonRescued();

        if ($person) {
            $form = $this->createForm(PersonType::class, $person);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $servicePerson->save($person);
                $this->addFlash(
                    'success',
                    'La personne a été crée avec succès'
                );
            }
        }

        return $this->render('app/person/create_person.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
