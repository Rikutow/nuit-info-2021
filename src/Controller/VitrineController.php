<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VitrineController extends AbstractController
{
    /**
     * @Route("/", name="vitrine_home")
     */
    public function home(): Response
    {
        return $this->render('vitrine/home/home.html.twig');
    }
}
