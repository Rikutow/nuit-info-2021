const InputContainerComponent = (() => {
    // add active class
    const handleFocus = (e) => {
        const target = e.target;
        target.parentNode.classList.add('active');
        target.setAttribute('placeholder', target.getAttribute('data-placeholder') || '');
    };

    // remove active class
    const handleBlur = (e) => {
        const target = e.target;
        if (!target.value) {
            target.parentNode.classList.remove('active');
        }
        target.removeAttribute('placeholder');
    };

    // register events
    const bindEvents = (element) => {
        const input = element.querySelector('input');
        input.addEventListener('focus', handleFocus);
        input.addEventListener('blur', handleBlur);
    };

    // get DOM elements
    const init = () => {
        const inputContainers = [...document.querySelectorAll('.input-row')];

        inputContainers.map((element) => {
            if (element.querySelector('input').value) {
                element.querySelector('.input-row__input-container').classList.add('active');
            }

            bindEvents(element);
        });
    };

    return {
        init: init,
    };
})();

InputContainerComponent.init();
