import React from "react";
import ReactDOM from "react-dom";

// import DevisComponent from './js/components/Devis';

// if (document.getElementById('root-devis')) ReactDOM.render(<DevisComponent />, document.getElementById('root-devis'));
import "./styles/components/_root.scss";
import "./styles/components/_colors.scss";
import "./styles/components/_button.scss";
import "./styles/components/_inputs.scss";
import "./styles/components/_link.scss";
import "./styles/components/_waves.scss";

import "./styles/specifics/_menu.scss";
import "./styles/specifics/_landing-page.scss";
import "./styles/specifics/_authentication.scss";
import "./styles/specifics/_home.scss";
import "./styles/specifics/_flash-messages.scss";
import "./styles/specifics/_boat.scss";
