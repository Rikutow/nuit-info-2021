import { url_prefix as URL, checkStatus } from "./utils.js";

let API = {
  getData: () => {
    return fetch(`${URL}/data`, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    })
      .then(checkStatus)
      .then((res) => res.json());
  },
};

export default API;
