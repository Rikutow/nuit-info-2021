export let checkStatus = (res) => {
  if (res.ok) {
    return res;
  } else {
    return res.text().then((msg) => {
      console.log(msg);
    });
  }
};

export const url_prefix = "localhost:8001";
