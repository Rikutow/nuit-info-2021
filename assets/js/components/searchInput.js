import React, { useEffect, useState } from "react";
import Select from "react-select";
import API from "../api";
import { useQuery } from "react-query";
import { FaSearch, FaTimes } from "react-icons/fa";
import { motion } from "framer-motion";

let SearchInput = () => {
  //TODO
  //let { isLoading, data: data } = useQuery("data", API.getData);
  const [searchData, setSearchData] = useState("");

  const variants = {
    visible: { opacity: 1, transition: { delay: 0.2 } },
    hidden: { opacity: 0 },
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
      }}
    >
      <input
        value={searchData}
        onChange={(e) => setSearchData(e.target.value)}
        placeholder={
          LOCALE === "en"
            ? "Search a rescuer, boat..."
            : "Rechercher un sauveteur, bateau..."
        }
        style={{
          borderRadius: "8px 8px 8px 8px",
          border: "none",
          borderColor: "#AAB8C2",
          height: "40px",
          backgroundColor: "#F5F8FA",
          width: "100%",
          textAlign: "left",
          fontSize: "20px",
          padding: "10px",
          paddingRight: "40px",
        }}
      ></input>
      <div
        style={{
          borderRadius: "0px 8px 8px 0px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          padding: "10px",
          paddingRight: "20px",
          position: "absolute",
          right: "0",
          pointerEvents: searchData ? "auto" : "none",
        }}
      >
        <motion.div
          style={{ position: "absolute", top: "0" }}
          variants={variants}
          initial="visible"
          animate={searchData ? "hidden" : "visible"}
        >
          <FaSearch color="#244245" size={20} />
        </motion.div>
        <motion.div
          style={{ position: "absolute", top: "0" }}
          variants={variants}
          initial="hidden"
          animate={searchData ? "visible" : "hidden"}
        >
          <FaTimes
            style={{ cursor: "pointer" }}
            onClick={() => setSearchData("")}
            color="#244245"
            size={20}
          ></FaTimes>
        </motion.div>
      </div>
    </div>
  );
};

export default SearchInput;
