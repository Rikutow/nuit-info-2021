import React from 'react';
import ReactDOM from 'react-dom';
import SearchInput from './components/searchInput';
import { QueryClient, QueryClientProvider } from 'react-query';

let App = () => {
    const queryClient = new QueryClient();
    return (
        <QueryClientProvider client={queryClient}>
            <SearchInput />
        </QueryClientProvider>
    );
};

ReactDOM.render(<App />, document.getElementById('app'));
