import React from 'react';
import ReactDOM from 'react-dom';
import TrashyOutput from './js/components/trashyOutput';

let Trash = () => {
    return <TrashyOutput />;
};

ReactDOM.render(<Trash />, document.getElementById('trash'));
